
function formValidation() {
    let firstName = document.getElementById('firstName').value;
    let surname = document.getElementById('surname').value;
    let email = document.getElementById('email').value;
    let message = document.getElementById('message').value;

    if (firstName === '' || surname === '' || email === '' || message === '') {
        alert('Required fields are not filled in.');
        return false;
    }

    let emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
        alert('Please enter a valid email address');
        return false;
    }

    return true;
}