function orderNowButton() {
    document.getElementById("orderNow").innerHTML = "You just ordered a sandwich.";
}

const blanchardstownAddress = {
    addressLine1: '1 Blanchardstown Road',
    addressLine2: 'Blanchardstown'
};
function blanchardstownLocation() {
    const blanchardstownLocationListing = document.getElementById("fullAddress");
    blanchardstownLocationListing.innerHTML =
    `<p>${blanchardstownAddress.addressLine1}</p>
    <p>${blanchardstownAddress.addressLine2}</p>`;
}

const clonskeaghAddress = {
    addressLine1: '1 Clonskeagh Road',
    addressLine2: 'Clonskeagh'
};
function clonskeaghLocation() {
    const clonskeaghLocationListing = document.getElementById("fullAddress");
    clonskeaghLocationListing.innerHTML =
    `<p>${clonskeaghAddress.addressLine1}</p>
    <p>${clonskeaghAddress.addressLine2}</p>`;
}

const grangegormanAddress = {
    addressLine1: '1 Grangegorman Road',
    addressLine2: 'Grangegorman'
};
function grangegormanLocation() {
    const grangegormanLocationListing = document.getElementById("fullAddress");
    grangegormanLocationListing.innerHTML =
    `<p>${grangegormanAddress.addressLine1}</p>
    <p>${grangegormanAddress.addressLine2}</p>`;
}

const swordsAddress = {
    addressLine1: '1 Swords Road',
    addressLine2: 'Swords'
};
function swordsLocation() {
    const swordsLocationListing = document.getElementById("fullAddress");
    swordsLocationListing.innerHTML =
    `<p>${swordsAddress.addressLine1}</p>
    <p>${swordsAddress.addressLine2}</p>`;
}

const parnellAddress = {
    addressLine1: '1 Parnell Street',
    addressLine2: 'Town or something idk'
};
function parnellLocation() {
    const parnellLocationListing = document.getElementById("fullAddress");
    parnellLocationListing.innerHTML =
    `<p>${parnellAddress.addressLine1}</p>
    <p>${parnellAddress.addressLine2}</p>`;
}

function orderNowForm()
{
    var orderNowForm = document.getElementById("orderNowForm").value
    if (orderNowForm == "sandwich") {
        document.getElementById("orderNowFormResult").innerHTML = "Good choice.";
    }
    else {
        document.getElementById("orderNowFormResult").innerHTML = "We don't do that here.";
    }
}

const breakfastItems = ["Breakfast Roll", "BLT", "Egg Mayo"];
const brunchItems = ["BLT", "Deli Meats and Mozzarella", "Ham and Mustard"];
const lunchItems = ["BLT", "Ham Sandwich", "Corned Beef Sandwich"];
const kidsItems = ["Ham Sandwich and Crisps", "Turkey and Stuffing", "PB&J"];

function displayBreakfastItems() {
    const breakfast = document.getElementById('breakfast');
    breakfastItems.forEach(breakfastItems => {
        const listItem = document.createElement('li');
        listItem.textContent = breakfastItems;
        breakfast.appendChild(listItem);
    });
}
displayBreakfastItems();

function displayBrunchItems() {
    const brunch = document.getElementById('brunch');
    brunchItems.forEach(brunchItems => {
        const listItem = document.createElement('li');
        listItem.textContent = brunchItems;
        brunch.appendChild(listItem);
    });
}
displayBrunchItems();

function displayLunchItems() {
    const lunch = document.getElementById('lunch');
    lunchItems.forEach(lunchItems => {
        const listItem = document.createElement('li');
        listItem.textContent = lunchItems;
        lunch.appendChild(listItem);
    });
}
displayLunchItems();

function displayKidsItems() {
    const kids = document.getElementById('kidsMenu');
    kidsItems.forEach(kidsItems => {
        const listItem = document.createElement('li');
        listItem.textContent = kidsItems;
        kids.appendChild(listItem);
    });
}
displayKidsItems();